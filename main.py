import pandas as pd
import numpy as np
from sklearn import linear_model

def get_data(fname):
    data = pd.read_csv(fname, delim_whitespace=True, header=None)
    X = data.as_matrix()[:, :2]
    y = data.as_matrix()[:, 2]
    return X, y
    
def transform(X):
    x1 = X[:, 0]
    x2 = X[:, 1]
    ones = np.ones(len(x1))
    return np.vstack((ones, x1, x2, x1**2, x2**2, x1 * x2, np.abs(x1 - x2), np.abs(x1 + x2))).T

def error(X, y, model):
    return 1 - model.score(X, y)

X_in, y_in = get_data('data/in.dta')
X_out, y_out = get_data('data/out.dta')

X_in = transform(X_in)
X_out = transform(X_out)

model = linear_model.LogisticRegression(penalty='none', solver='lbfgs').fit(X_in, y_in)
E_in = error(X_in, y_in, model)
E_out = error(X_out, y_out, model)
print(f'E_in: {E_in}\tE_out: {E_out}')

model_reg = linear_model.LogisticRegression(penalty='l2', C=1000, solver='lbfgs').fit(X_in, y_in)
E_in = error(X_in, y_in, model_reg)
E_out = error(X_out, y_out, model_reg)
print(f'E_in: {E_in}\tE_out: {E_out}')